package com.example.parrot.wearosbasics;

import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends WearableActivity {

    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextView = (TextView) findViewById(R.id.textLabel);

        // Enables Always-on
        setAmbientEnabled();
    }

    public void buttonPressed(View view) {

        // 1. Get message from the text label
        String message = mTextView.getText().toString();

        // 2. Change the message
        if (message == "Hello!") {
            mTextView.setText("Goodbye!");
        }
        else {
            mTextView.setText("Hello!");
        }
    }
}
